# Introduction

Technologies used:

- Apollo
- GraphQl
- MongoDB
- NodeJS

Features:
- Subscription 
- Send Notifications







# Part 0: The Setup

## Setting up Express
1. Create /server, add .gitignore, npm init
2. yarn add:
    - express
    - body-parser
    - cross-env : ensures consistent ENV variable behaviour across OS
    - nodemon : cycler testing for file changes
3. Setup app.js 
4. yarn add -D: 
    - babel-plugin-transform-object-rest-spread
    - babel-cli (you can do this globally or locally, but you want to do it locally to remove dependency on your personal environemnt and ensure portability of env)
    - babel-preset-env
5. Create .babelrc 
6. Change script to use "babel-node", to use in place for "node" command
7. Add to package.json scripts "dev": "cross-env NODE_ENV=dev nodemon --exec babel-node src/app.js"

## Setting up MongoDB

1. Setup the database e.g. Mlab DBaaS
2. yarn add mongoose
3. Create config/constants.js and add DB constants
4. Create config/db.js and init DB
5. In app.js, do: import "config/db"






# Part 1: Setting up GraphQL

## Creating models, graphQL Schema 

1. Create /models/Tweet.js and export Tweet model
2. Create /graphql/schema.js (describe your app's data to graphQL)
3. Create /graphql/resolvers > index.js AND tweet-resolvers.js (interface your queries with MongoDB)

## Pipe up graphQL with apollo-server-express

1. yarn add:
    - graphql
    - apollo-server-express (connects up all our /graphql files - currently they do nothing!)
    - graphql-tools 
4. app.js changes to install graphiql(IDE) and boot up graphQL. ENSURE bodyParser is used before graphQL server is used as it needs access to body values

## Create mock data

1. create /mocks with fake data
2. yarn add faker 

## One sentence on graphQL vs REST

- With REST APIs you often needed to do multiple requests to do simple things
- With GraphQL you do one query, get one result

## /graphql/schema.js

- This is where you teach graphQL about your app
- Everything in REST was GET, in graphQL you have 'Query'

- resolvers are required to connect these queries with MongoDB
- i.e. this is where MongoDB model methods are called 

## app.js changes

```js
// make an executable schema
const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

// setup graphQL server
app.use( constant.GRAPHQL_PATH, graphqlExpress({
    schema
}) );

// create the new server from the app
const graphQLServer = createServer( app );

// change app.listen to graphQLServer.listen()
graphQLServer.listen( constant.PORT, err => {
    if( err ) {
        console.error( err );
    } else {
        console.log( `App listening on port ${ constant.PORT }` );
    }
} );
```

## Example graphQL query

```
{
  getTweets {
    _id
    text
  }
}
```







# Part 2: CRUD Operations 


## Get Tweet

1. Add getTweet( _id : ID ) to Query in /graphql/schema.js
2. Add getTweet in /graphql/resolvers/tweet-resolvers.js => can now be queried (see query below)

### Arguments in graphQL

- ! means it is required, or graphql will abort operation
- resolver args ( parent, args ) ---- parent is optional (discussed later), and args is destructured e.g. { _id }

### Example graphQL query to get a tweet (with arg)

```
{
  getTweet( _id : "5b4b5a497e29812b8f430d0c" ) {
    _id,
    text
  }
}
```


## Creating a Tweet

1. Add type Mutation {} to /graphql/schema.js (dont forget to add to "schema" key too)
2. Add createTweet signature in this object
2. Add createTweet resolver

Note:

- Query is all about GET
- Mutation is all other update operations

### Calling Mutation

```
mutation {
    createTweet( text : "Hello there" ) {
        _id
        text
    }
}
```


## Updating a Tweet

1. Add updateTweet signature under type Mutation {} in /graphql/schema.js
2. Add updateTweet resolver

### Calling Mutation

```
mutation {
    updateTweet( id : "5b123bwerw2kb2kb", text : "New text!" ) {
        _id
        text
    }
}
```

### The common catch with updates!

- As soon as mutation is called, server updates, however the returned object is still old (off by one update!)
- To get new, you need to specify the { new : true } option for findByIdAndUpdate in updateTweet resolver


## Deleting a Tweet 

1. Add Status into graphQL schema (no Tweet to return, so add Status to Schema)
2. Add the deleteTweet signature under type Mutation {} in /graphql/schema.js and return Status( deleteTweet( _id : ID ) : Status )
3. Add the deleteTweet resolver

```
mutation {
    deleteTweet( _id : "5b4b6d311a1f682fbb545e34" ) {
        message
    }
}
```

### Getting tweets by timeorder

1. Add { timestamp : true } option to Tweet model Schema => adds "createdAt" and "updatedAt"
2. Tweet resolver for getTweets fn, Tweet.find({}).sort({ createdAt : -1 })

Making sure dates are Dates not Strings

i. Add 'scalar Date' to graphql/schema.js
ii. yarn add graphql-date
iii. Add Date definition in resolvers.js by...
iv. import GraphQLDate from "graphql-date" + adding it with key "Date" to exported resolvers
v. From now on, when Date is referenced in graphql, it will check the canonical typedefs







# Part 3 : Adding User Model

## Adding the model

Pre-step: Install separate GraphIQL GUI (https://github.com/skevy/graphiql-app). This is an electron app which combines graphiql with ability to edit HTTP Headers.

1. Create User Model
2. Add type def for user in graphql schema
3. Create User Resolver : with signup method
4. Ensure you update your mocks! (I.e. remove all mock users)


## Storing the password on DB as a hash (i.e. not directly!)

1. yarn add bcrypt-nodejs
2. import { hashSync } from "bcrypt-nodejs" in User Model
3. In UserSchema.methods, add a function called _hashPassword (can be called on each user)
4. Add UserSchema.pre( "save", cb(next) { ... } )
5. If this.isModified( "password" ), set this.password = this._hashPassword( this.password ). Continue up the chain.

## Authenticate User

1. import { compareSync } from "bcrypt-nodejs" in User Model
2. Add to UserSchema.methods, a function called authenticateUser (can be called on each user) => compareSync( password, this.password ) => BOOL
3. In User resolver, add a login method which takes { email, password }
    i. Test for user existence
    ii. Test for user.authenticateUser( password )
    iii return User for now
4. Ensure graphQL schema has access to this resolver






# Part 4 : JWT authentication, and controlling access to authed routes

## Creating the Auth type
1. Create type Auth {} in graphql/schema
2. With the single field of { token : String! }
3. Make the 'signUp' and 'login' mutation return Auth (type)

## Signing with JWT
4. Create a method on User to create auth token 
    i. yarn add jsonwebtoken 
    ii. import jwt from "jsonwebtoken";
    iii. add in UserSchema.methods, createToken() which calls jwt.sign( { _id : this.id }, JWT_SECRET )
    iv. add JWT_SECRET to constants
5. In user-resolvers, get the newly created user and return { token : user.createToken() } on completion of both 'signUp' and 'login' 

## Creating auth service methods, and storing auth on context
6. Create auth.js into a new folder src/services
7. Create decodeToken( token )
8. Re-organise code: Create /config/middleware folder, which exports middlewares( app )
9. Re-organise code: Move app.js middlewares into this folder
10. Create auth(req,res,next) middleware, and app.use( auth ). If auth passes, add the returned user to req.user - this will now be added to graphQL's context
11. Add callback to graphqlExpress, and add "context" key to make values available to graphQL

## Accessing the context
12. In tweet-resolvers > createTweet, add third arg 'ctx'
13. In headers of createTweet, set 'authorization' key to "Bearer" (token prefix defined in services/auth) 
14. On createTweet you will then have access to context from step 11.

## Testing auth before resolvers can interact with DB
15. In services/auth, create async requireAuth( user ) which tests user
16. Call this in all methods where auth is required in e.g. tweet-resolvers.js whenever you want to ensure auth on a method

## Creating a type Me
17. In schema.js Copy and paste { User } to { Me }, and add to Query {} in same file
18. Add the me method under resolvers/user-resolvers.js (takes logged-in user from context and interfaces with DB to get the User instance)
19. Add to resolvers/index.js the key-value { me : Me }, 

## Handling case where decoded JWTs work forever (e.g. If user no longer exists)
20. In services/auth > requireAuth(), add another test to check DB for user using User.findById( .. ). This ensures you have JWT of existing user

### JWT explained

Contains:
1. Headers : The Hash Algorithm used + other meta-info
2. Payload : Usually the user object
3. Signature






# Part 5 : Linking Tweets to a User, and only allowing them to modify/delete their own tweets

1. Aside: change Tweet schema's text property to object with { type : String, minlength : [ 5, "MSG" ], maxlength : [ 140, "MSG" ] }
2. Change Tweet schema to include User with value { type : Schema.Types.ObjectId, ref : "User" } --- "User" is the string assigned in mongoose.model( "User", UserSchema )
3. Add favoriteCount 
4. Change graphQL Schema > type Tweet to return { user : User! }
5. On tweet-resolvers > createTweet, we need to include context's user arg into the createTweets parms, so change parms to { ... args, user : user._id } 
6. *However*, Tweet Schema cannot return User object *until* you add Tweet : { user : ... } into resolvers/index.js - the user key holds a fn which gets passed the parent tweet, where the user var holds the user's id
7. Change mocks to add users, and create tweets from multiple users
8. Need to ensure that tweet-resolvers > updateTweet() *only* allows you to update tweets that are yours, thus you have to:
    i. use the context's user as a search param on tweets + throw error if that tweet doesn't exist
    iii. otherwise change values on that tweet
    iv. and then return await tweet.save() at the end
9. Same for tweet-resolvers > deleteTweet()
    i. use the context's user as a search param on tweets + throw error if that tweet doesn't exist
    ii. call await tweet.remove()
10. Create tweet-resolvers > getUserTweets to return only your tweets