import React, { Component } from "react";
import styled from "styled-components";
import Touchable from "@appandflow/touchable";

import SignUpForm from "../components/SignUpForm";
import LoginForm from "../components/LoginForm";
import LottieView from 'lottie-react-native';

const Root = styled.View`
    flex : 1;
    align-items : center;
    justify-content : center;
    background-color : ${ p => p.theme.SECONDARY };
`;

const Text = styled.Text`
    color : white;
`;

const Button = styled( Touchable ).attrs({
    feedback : "highlight"
})`
    height : 60;
    background-color : ${ p => p.theme.PRIMARY };
    align-items : center;
    justify-content : center;
    position : absolute;
    bottom : 70;
    width : 90%;
    left : 5%;
    border-radius : 3;
`;

const BottomButtonContainer = styled.View`
    position : absolute;
    width : 100%;
    bottom : 10;
    display : flex;
    align-items : center;
    justify-content : center;
`;

const BottomButton = styled( Touchable ).attrs({
    feedback : "opacity"
})`
    padding : 20px;
`;

const initialState = {
    showSignUpForm : false,
    showLoginForm : false
};

export default class extends Component {
    constructor() {
        super();
        this.state = initialState;
        this._onRegisterPress = this._onRegisterPress.bind(this);
        this._onLoginPress = this._onLoginPress.bind(this);
        this._onBackPress = this._onBackPress.bind(this);
    }

    _onRegisterPress() {
        this.setState({ showSignUpForm : true });   
    }

    _onLoginPress() {
        this.setState({ showLoginForm : true });
    }
    
    _onBackPress() {
        this.setState( initialState );   
    }

    render() {
        if( this.state.showSignUpForm ) 
            return <SignUpForm onBackPress={ this._onBackPress } />;

        if( this.state.showLoginForm )
            return <LoginForm onBackPress={ this._onBackPress } />;

        return (
            <Root>
                <LottieView 
                    source={require("../../assets/lottie/twitter-bird.json")} 
                    autoPlay 
                    loop={ false }
                    style={{ width : "50%"}} />

                <Button onPress={ this._onRegisterPress }>
                    <Text>Get Started</Text>
                </Button>
                <BottomButtonContainer>
                    <BottomButton onPress={ this._onLoginPress }>
                        <Text>Already a member</Text>
                    </BottomButton>
                </BottomButtonContainer>
            </Root>
        );
    }
}