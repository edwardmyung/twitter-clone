import React, { Component } from "react";
import styled from "styled-components";
import { graphql } from "react-apollo";
import { ActivityIndicator, FlatList } from "react-native";

import TweetCard from "../components/TweetCard";
import GET_TWEETS_QUERY from "../graphql/queries/getTweets.js";

const Root = styled.View`
    flex : 1;
`;

class HomeScreen extends Component {

    _renderItem({ item }) {
        return <TweetCard { ... item } />;
    }

    render() {
        const { data } = this.props;

        if( data.loading ) {
            return (
                <Root>
                    <ActivityIndicator size="large"/>
                </Root>
            );
        }

        return (
            <Root>
                <FlatList
                    data={ data.getTweets }
                    keyExtractor={ item => item._id }
                    renderItem={ this._renderItem } />
            </Root>
        );
    }
}

export default graphql( GET_TWEETS_QUERY )( HomeScreen );