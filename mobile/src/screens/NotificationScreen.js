import React, { Component } from "react";
import styled from "styled-components";

const Root = styled.View`
    flex : 1;
`;

const Text = styled.Text``;

export default class extends Component {
    static navigationOptions = {
        title : "Notifications"
    }

    render() {
        return (
            <Root>
                <Text>Notifications</Text>
            </Root>
        );
    }
}