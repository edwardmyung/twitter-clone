import React, { Component } from "react";
import styled from "styled-components";

import { graphql } from "react-apollo";
import ME_QUERY from "../graphql/queries/me";

import Loading from "../components/Loading";

const PROFILE_IMAGE_SIZE = 150;

const Root = styled.View`
    flex : 1;
`;

const Banner = styled.View`
    width : 100%;
    background : red;
    height : 180;
    display:flex;
    justify-content : flex-end;
    align-items:center;
    background: #6190E8;
`;

const ProfileImage = styled.Image`
    width : ${PROFILE_IMAGE_SIZE};
    height : ${PROFILE_IMAGE_SIZE};
    border-radius:10;
    margin-bottom : -${PROFILE_IMAGE_SIZE/2};
`;

const ContentWrapper = styled.View`
    padding-top : ${(PROFILE_IMAGE_SIZE/2) + 20};
    display: flex;
    align-items :center;
`;

const TextFullName = styled.Text`
    font-weight : 600;
`;

const TextUsername = styled.Text`
`;

class ProfileScreen extends Component {
    static navigationOptions = {
        title : "Profile"
    }

    render() {
        const { data } = this.props;
        if (data.loading ) {
            return <Loading />
        }

        return (
            <Root>
                <Banner>
                    <ProfileImage source={{ uri: data.me.avatar }}/>
                </Banner>
                <ContentWrapper>
                    <TextFullName>{ data.me.firstName } { data.me.lastName }</TextFullName>
                    <TextUsername>@{ data.me.username }</TextUsername>
                </ContentWrapper>
            </Root>
        );
    }
}

export default graphql(ME_QUERY)(ProfileScreen);