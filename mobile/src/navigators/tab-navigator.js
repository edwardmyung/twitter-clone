import React from "react";
import { createBottomTabNavigator } from "react-navigation";
import { FontAwesome } from "@expo/vector-icons";

import HomeScreen from "../screens/HomeScreen";
import ExploreScreen from "../screens/ExploreScreen";
import NotificationScreen from "../screens/NotificationScreen";
import ProfileScreen from "../screens/ProfileScreen";

import { colors } from "../utils/constants"; 

const ICON_SIZE = 20;

export default createBottomTabNavigator(
    {
        Home : { 
            screen : HomeScreen,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => <FontAwesome name="home" size={ ICON_SIZE } color={ tintColor } />
            })
        },
        Explore : { 
            screen : ExploreScreen,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => <FontAwesome name="search" size={ ICON_SIZE } color={ tintColor } />
            })
        },
        Notifications : { 
            screen : NotificationScreen,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => <FontAwesome name="bell" size={ ICON_SIZE } color={ tintColor } />
            })
        },
        Profile : { 
            screen : ProfileScreen,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => <FontAwesome name="user" size={ ICON_SIZE } color={ tintColor } />
            })
        }
    },
    {
        lazy : true,
        tabBarPosition : "bottom",      // important to force for android
        swipeEnabled : false,           // prevents swiping through tabs
        tabBarOptions : {
            showIcon : true,
            showLabel : true,
            activeTintColor : colors.PRIMARY,
            inactiveTintColor : colors.LIGHT_GRAY,
            style : {
                backgroundColor : colors.WHITE,
                height : 50,
                paddingVertical : 5
            }
        }
    }
);