import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";

import TabNavigator from "./tab-navigator";
import AuthenticationScreen from "../screens/AuthenticationScreen";

import { connect } from "react-redux";

// Create the stack navigator, remember this fn => a Component
const AppMainStackNavigator = createStackNavigator(
    {
        Home : { 
            screen : TabNavigator,
            navigationOptions : ({ navigation }) => {
                let { routeName } = navigation.state.routes[ navigation.state.index ];
                return { title : routeName };
            }
        }
    },

    // === OPTIONS ===
    {

        initialRouteName: "Home",

        cardStyle : { 
            backgroundColor : "#f2f2f2" 
        },

        navigationOptions : {
            headerStyle : { backgroundColor : "white" },
            headerTintColor : "#5a5a5a",
            headerTitleStyle : { fontSize : 14 }
        }
    }
);

// This tests the user.isAuthenticated from the store
class AppNavigator extends Component {
    render() {
        var { user } = this.props;

        if( !user.isAuthenticated )
            return <AuthenticationScreen />;

        return <AppMainStackNavigator />;
    }
}


var mapStateToProps = ( s ) => ({ user : s.user });
export default connect( mapStateToProps )( AppNavigator );