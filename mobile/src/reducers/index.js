import { combineReducers } from "redux";
import user from "./user";

export default client => combineReducers({
    apollo: client.reducer(),
    user
});