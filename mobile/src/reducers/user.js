const initialState = {
    isAuthenticated : false,
    info : null // will be object, but null better than {} (as {} => true)
};

export default ( state = initialState, action ) => {
    if( action.type === "LOGIN" ) {
        return {
            ... state,
            isAuthenticated : true
        };
    }

    // default case
    return state;
};