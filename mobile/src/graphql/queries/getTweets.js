import { gql } from "react-apollo";

export default gql`
    {
        getTweets {
            text
            _id
            createdAt
            favoriteCount
            isFavorited
            user {
                username
                avatar
                lastName
                firstName
            }
        }
    }
`;