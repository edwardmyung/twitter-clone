import { gql } from "react-apollo";

export default gql`
    mutation signUp( 
        $fullName : String!, 
        $email : String!, 
        $password : String!, 
        $username : String!, 
        $avatar : String!
    ) {
        signUp( 
            fullName : $fullName, 
            email : $email, 
            password : $password, 
            username : $username, 
            avatar : $avatar
        ) {
            token
        }
    }
`;