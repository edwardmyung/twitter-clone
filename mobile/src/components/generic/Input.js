import React from "react";
import styled from "styled-components";
import {colors} from "../../utils/constants";


const Input = styled.TextInput.attrs({
    placeholderTextColor : colors.LIGHT_GRAY
})`
    height : 35;
    width : 100%;
    margin-bottom : 10px;
    padding-left:10;
    color : ${ props => props.theme.BLACK };
`;


export default (props) => {
    return (
        <Input {...props}/>
    )
}