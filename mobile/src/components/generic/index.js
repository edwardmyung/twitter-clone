import {default as InputComponent} from "./Input";
import {default as ButtonComponent} from "./Button";

export const Input = InputComponent;
export const Button = ButtonComponent;