import React from "react";
import styled from "styled-components";
import Touchable from "@appandflow/touchable";

const Wrapper = styled( Touchable ).attrs({
    feedback : "opacity"
})`
    height : 60;
    background-color : ${ p => p.theme.PRIMARY };
    align-items : center;
    justify-content : center;
    width : 100%;
    border-radius : 3;
`;

export default ({ children, ...rest }) => {
    return (
        <Wrapper {...rest}>
            { children }
        </Wrapper>
    );
}