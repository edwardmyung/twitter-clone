import React, { Component } from "react";
import {AsyncStorage, Text} from "react-native";
import styled from "styled-components";
import { connect } from "react-redux";
import { compose, graphql} from "react-apollo";

import { colors } from "../utils/constants";
import { login } from "../actions/user";
import LOGIN_MUTATION from "../graphql/mutations/login";

import Touchable from "@appandflow/touchable";
import { MaterialIcons } from "@expo/vector-icons";
import {Button, Input} from "./generic";

const Root = styled.View`
    flex : 1;
    justify-content : center;
    align-items : center;
`;

const Wrapper = styled.View`
    width:90%;
`


const BackButton = styled( Touchable ).attrs({
    feedback : "opacity"
})` 
    position : absolute;
    top : 8%;
    left : 8%;
`;
class LoginForm extends Component {
    
    constructor( props ){
        super( props );
        this.state = {
            loading : false
        };

        this._onChangeText = this._onChangeText.bind(this);
        this._onLoginPress = this._onLoginPress.bind(this);
    }

    _onChangeText( val, stateKey ) {
        this.setState({
            [ stateKey ] : val
        });
    }

    async _onLoginPress() {
        this.setState({ loading : true });
        
        const { username, password } = this.state;

        try {
            const { data } = await this.props.mutate({
                variables : {
                    username,
                    password
                }
            });

            AsyncStorage.setItem( "twitterclone/authToken", data.login.token );
            this.setState({ loading : false });
            return this.props.login(); // returns the action { type : "LOGIN" }
        } catch ( err ) {
            throw err;
        }
    }


    _checkIfDisabled() {
        const { username,  password } = this.state;

        if( !username || !password )
            return true;

        return false;
    }

    render() {
        return (
            <Root>
                <BackButton onPress={ this.props.onBackPress }>
                    <MaterialIcons color={ colors.name } name="arrow-back" size={ 20 }/>
                </BackButton>
                <Wrapper>
                    <Input placeholder="Username"
                        autoCapitalize="none"
                        onChangeText={ text => this._onChangeText( text, "username" ) }/>
                    
                    <Input placeholder="Password"
                        secureTextEntry
                        onChangeText={ text => this._onChangeText( text, "password" ) }/>
                    
                    <Button 
                        onPress={ this._onLoginPress }
                        disabled={ this._checkIfDisabled() }>
                        <Text style={{ color : "white" }}>Login</Text>
                    </Button>
                </Wrapper>
            </Root>
        );
    }
}



export default compose(
    graphql( LOGIN_MUTATION ),
    connect( null, { login } )
)( LoginForm );