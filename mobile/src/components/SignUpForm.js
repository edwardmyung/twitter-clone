import React from "react";
import { 
    Keyboard,
    TouchableWithoutFeedback,
    AsyncStorage,
    Text
} from "react-native";
import { connect } from "react-redux";
import { compose, graphql } from "react-apollo";
import styled from "styled-components";

import SIGNUP_MUTATION from "../graphql/mutations/signup";
import { login } from "../actions/user";
import { colors } from "../utils/constants";

import Loading from "./Loading";
import Touchable from "@appandflow/touchable";
import { MaterialIcons } from "@expo/vector-icons";
import { Input, Button } from "./generic";

const Root = styled.View`
    flex : 1;
    justify-content : center;
    align-items : center;
`;

const Wrapper = styled.View`
    align-items : center;
    width : 90%;
`;

const BackButton = styled( Touchable ).attrs({
    feedback : "opacity"
})` 
    position : absolute;
    top : 8%;
    left : 8%;
`;

class SignUpForm extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            fullName : "",
            email : "",
            password : "",
            username : "",
            loading : false
        };

        this._onSignUpPress = this._onSignUpPress.bind(this);
        this._onChangeText = this._onChangeText.bind(this);
    }

    _onOutsidePress() {
        Keyboard.dismiss();
    }

    _onChangeText( val, stateKey ) {
        this.setState({ [ stateKey ] : val });
    }

    _checkIfDisabled() {
        const { fullName, email, password, username } = this.state;
        const someFieldsEmpty = !fullName || !email || !password || !username;

        if( someFieldsEmpty )  
            return true;

        return false;
    }

    async _onSignUpPress() {
        this.setState({ loading : true });
        
        const { fullName, email, password, username } = this.state;
        const avatar = "https://pbs.twimg.com/profile_images/956594738127360000/gzQO5JzI_400x400.jpg";

        try {
            const { data } = await this.props.mutate({
                variables : {
                    fullName,
                    email,
                    password,
                    username,
                    avatar
                }
            });

            AsyncStorage.setItem( "twitterclone/authToken", data.signUp.token );
            this.setState({ loading : false });
            return this.props.login(); // returns the action { type : "LOGIN" }
        } catch ( err ) {
            throw err;
        }
    }

    render() {
        if( this.state.loading ) 
            return <Loading />;

        return (
            <TouchableWithoutFeedback onPress={ this._onOutsidePress }>
                <Root>
                    <BackButton onPress={ this.props.onBackPress }>
                        <MaterialIcons color={ colors.name } name="arrow-back" size={ 20 }/>
                    </BackButton>
                    <Wrapper>
                        <Input 
                            placeholder="Full name"
                            autoCapitalize="words"
                            onChangeText={ text => this._onChangeText( text, "fullName" ) }
                            autoFocus/>
                        <Input 
                            placeholder="Email"
                            keyboardType="email-address"
                            onChangeText={ text => this._onChangeText( text, "email" ) }/>
                    
                        <Input 
                            placeholder="Password"
                            secureTextEntry
                            onChangeText={ text => this._onChangeText( text, "password" ) } />
                        <Input 
                            placeholder="Username"
                            autoCapitalize="none"
                            onChangeText={ text => this._onChangeText( text, "username" ) }/>
                        <Button 
                            onPress={ this._onSignUpPress }
                            disabled={ this._checkIfDisabled() }>
                            <Text style={{ color : "white" }}>Register</Text>
                        </Button>
                    </Wrapper>
                </Root>
            </TouchableWithoutFeedback>
        );
    }
}

export default compose(
    graphql( SIGNUP_MUTATION ),
    connect( null, { login } )
)( SignUpForm );