import React from "react";
import styled from "styled-components";

const Container = styled.View`
    flex : 1;
    padding : 10px 0px;
`;

const CardText = styled.Text`
    font-size : 14;
    color : ${ props => props.theme.SECONDARY };
`;

const Body = ({ text }) => (
    <Container>
        <CardText>{ text }</CardText>
    </Container>
);

export default Body;