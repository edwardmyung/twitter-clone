import React from "react";
import styled from "styled-components/native";
import { graphql, gql } from  "react-apollo";

import FAVORITE_TWEET_MUTATION from "../../graphql/mutations/favoriteTweet"

import Header from "./Header";
import Body from "./Body";
import Footer from "./Footer";

const Wrapper = styled.View`
    min-height : 180;
    width : 100%;
    padding : 7px;
    margin : 5px 0px;
    background : white;
    box-shadow: 0px 2px 2px rgba(0,0,0,0.1);
`;


const TweetCard = (props) => {
    const { user, text, createdAt, favoriteCount, isFavorited, onFavorite } = props;
    return (
        <Wrapper>
            <Header user={ user } createdAt={ createdAt }/>
            <Body text={ text }/>
            <Footer favoriteCount={ favoriteCount } isFavorited={ isFavorited } onFavorite={ onFavorite }/>
        </Wrapper>
    );
}



// TweetCard.fragments = {
//     tweet: gql`
//         fragment TweetCard on Tweet {
//             text
//             _id
//             createdAt
//             isFavorited
//             favoriteCount
//             user {
//                 username
//                 avatar
//                 lastName
//                 firstName
//             }
//         }
//     `
// }

export default graphql(FAVORITE_TWEET_MUTATION, {
    props: ({ ownProps, mutate }) => ({
        onFavorite: () => {
            const variables = { _id: ownProps._id };
            const optimisticResponse = {
                __typename: 'Mutation',
                favoriteTweet: {
                    __typename: 'Tweet',
                    _id: ownProps._id,
                    favoriteCount: ownProps.isFavorited
                        ? ownProps.favoriteCount - 1
                        : ownProps.favoriteCount + 1,
                    isFavorited: !ownProps.isFavorited,
                },
            };
            return mutate({
                variables,
                optimisticResponse
            })
        },
    }),
})(TweetCard);
  