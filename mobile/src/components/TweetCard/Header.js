import React from "react";
import styled from "styled-components";
import moment from "moment-timezone";

// border-radius is stricter than web -> must be done correctly!
const AVATAR_SIZE = 40;
const AVATAR_RADIUS = AVATAR_SIZE / 2;

const Root = styled.View`
    height : 50;
    flex-direction : row;
`;

const AvatarContainer = styled.View`
    width : 50;
    margin-right : 12;
    align-items : center;
    justify-content : center;
`;

const MetaContainer = styled.View`
    flex : 1;
`;

const MetaTop = styled.View`
    flex : 1;
    flex-direction : row;
    align-items : center;
`;

const MetaBottom = styled.View`
    flex : 0.8;
    justify-content : center;
`;

const MetaHeader = styled.Text`
    font-size : 16;
    font-weight : 500;
    color : ${ props => props.theme.SECONDARY };
`;

const MetaText = styled.Text`
    font-size : 14;
    font-weight : 600;
    color : ${ props => props.theme.LIGHT_GRAY };
`;

const Avatar = styled.Image`
    height : ${ AVATAR_SIZE };
    width : ${ AVATAR_SIZE };
    border-radius : ${ AVATAR_RADIUS };
`;

const src = "https://pbs.twimg.com/profile_images/956594738127360000/gzQO5JzI_400x400.jpg";

const Header = ({ user, createdAt }) => (
    <Root>
        <AvatarContainer>
            <Avatar source={{ uri : user.avatar || src }}/>
        </AvatarContainer>
        <MetaContainer>
            <MetaTop>
                <MetaHeader>{ user.firstName } { user.lastName }</MetaHeader>
                <MetaText style={{ marginLeft : 5 }}>@{ user.username }</MetaText>
            </MetaTop>
            <MetaBottom>
                <MetaText>{ moment( createdAt ).fromNow() }</MetaText>
            </MetaBottom>
        </MetaContainer>
    </Root>
);

export default Header;