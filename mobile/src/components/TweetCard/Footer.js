import React from "react";
import styled from "styled-components";
import { SimpleLineIcons, Entypo } from "@expo/vector-icons";
import Touchable from "@appandflow/touchable";

import { colors } from "../../utils/constants";


const Root = styled.View`
    height : 40;
    flex-direction : row;
`;

const TouchableButton = styled( Touchable ).attrs({
    feedback : "opacity"
})`
    flex : 1; 
    flex-direction : row;
    justify-content : center;
    align-items : center;
`;

const ButtonText = styled.Text`
    font-size : 14;
    font-weight : 500;
    margin-left : 6px;
    color : ${ p => p.theme.LIGHT_GRAY };
    flex-direction : row;
`;

// const isFavorited = true;

const Footer = ({ favoriteCount, isFavorited, onFavorite }) => (
    <Root>
        <TouchableButton>
            <SimpleLineIcons name="bubble" size={ 20 } color={ colors.LIGHT_GRAY }/>
            <ButtonText>{ favoriteCount }</ButtonText>
        </TouchableButton>
        <TouchableButton>
            <Entypo name="retweet" size={ 20 } color={ colors.LIGHT_GRAY }/>
            <ButtonText>{ favoriteCount }</ButtonText>
        </TouchableButton>
        <TouchableButton onPress={ onFavorite }>
            <Entypo 
                name={ isFavorited ? "heart" : "heart-outlined" }
                size={ 20 } 
                color={ isFavorited ? "red" : colors.LIGHT_GRAY }/>
            <ButtonText>{ favoriteCount }</ButtonText>
        </TouchableButton>
    </Root>
);

export default Footer;