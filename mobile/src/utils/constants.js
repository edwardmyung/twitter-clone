export const colors = {
    PRIMARY   : "#55ACEE",
    SECONDARY : "#444B52",
    WHITE     : "#FFFFFF",
    BLACK     : "#000000",
    LIGHT_GRAY: "#CAD0D6",
};