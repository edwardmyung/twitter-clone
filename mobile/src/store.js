import {AsyncStorage} from "react-native";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import ApolloClient, { createNetworkInterface } from "apollo-client";
import thunk from "redux-thunk";
import { logger } from "redux-logger";

import reducers from "./reducers";

const networkInterface = createNetworkInterface({
    uri: "http://localhost:3000/graphql",
});

export const client = new ApolloClient({
    networkInterface,
});

networkInterface.use([{
    applyMiddleware: async (req, next) => {
        if(!req.options.headers) {
            req.options.headers = {};
        }

        try {
            const token = await AsyncStorage.getItem("twitterclone/authToken");
            
            if(token) {
                req.options.headers.authorization = `Bearer ${ token }` || null;
            }
        } catch(err) {
            throw err;
        }

        return next();
    }
}])

const middlewares = [
    client.middleware(),
    thunk,
    logger
];

export const store = createStore(
    reducers( client ), // If you see reducer, apollo requires you to pass in client
    undefined,
    composeWithDevTools( applyMiddleware( ... middlewares ) ),
);