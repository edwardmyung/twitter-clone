import express from "express";
import {createServer} from "http";

import "./config/db";
import constant from "./config/constant";
import middleware from "./config/middleware";

/*

--- Temporarily removed mocks ---
To add back in:
1. import mocks from "./mocks";
2. Wrap graphQLServer.listen in:

```
mocks().then( () => {
    ... 
});
```
*/

const app = express();

middleware(app);

// create the new server from the app
const graphQLServer = createServer(app);

graphQLServer.listen(constant.PORT, err => {
    if (err) {
        console.error(err);
    } else {
        console.log(`App listening on port ${constant.PORT}`);
    }
});
