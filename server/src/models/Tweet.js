import mongoose, {Schema} from "mongoose";

const TweetSchema = new Schema(
    {
        text: {
            type: String,
            minlength: [5, "Tweet must be longer than 5 characters"],
            maxlength: [140, "Tweet must not be longer than 140 characters"]
        },

        user: {
            type: Schema.Types.ObjectId,
            ref: "User"
        },

        favoriteCount: {
            type: Number,
            default: 0
        }
    },
    {timestamps: true}
);

TweetSchema.statics = {
    incrementFavoriteCount(tweetId) {
        return this.findByIdAndUpdate(tweetId, {$inc: {favoriteCount: 1}}, {new: true});
    },

    decrementFavoriteCount(tweetId) {
        return this.findByIdAndUpdate(tweetId, {$inc: {favoriteCount: -1}}, {new: true});
    }
};

export default mongoose.model("Tweet", TweetSchema);
