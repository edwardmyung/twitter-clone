import mongoose, {Schema} from "mongoose";
import Tweet from "./Tweet";
import {pubSub} from "../config/pub-sub";
import {TWEET_FAVORITED} from "../graphql/resolvers/tweet-resolvers";

const TweetFavoriteSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    tweets: [{type: Schema.Types.ObjectId, ref: "Tweet"}]
});

TweetFavoriteSchema.methods = {
    async userFavoritedTweet(tweetId) {
        if (this.tweets.some(tweet => tweet.equals(tweetId))) {
            // Unfavoriting branch
            this.tweets.pull(tweetId);
            await this.save();
            const tweetRaw = await Tweet.decrementFavoriteCount(tweetId);
            const tweetJson = tweetRaw.toJSON();

            pubSub.publish(TWEET_FAVORITED, {[TWEET_FAVORITED]: {...tweetJson}});
            return {
                isFavorited: false,
                ...tweetJson
            };
        }

        // Favoriting branch
        const tweetRaw = await Tweet.incrementFavoriteCount(tweetId);
        const tweetJson = tweetRaw.toJSON();
        this.tweets.push(tweetId);
        await this.save();
        pubSub.publish(TWEET_FAVORITED, {[TWEET_FAVORITED]: {...tweetJson}});
        return {
            isFavorited: true,
            ...tweetJson
        };
    }
};

TweetFavoriteSchema.index({userId: 1}, {unique: true});

export default mongoose.model("TweetFavorite", TweetFavoriteSchema);
