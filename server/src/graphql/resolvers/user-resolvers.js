import User from "../../models/User";
import TweetFavorite from "../../models/TweetFavorite";
import {requireAuth} from "../../services/auth";

export default {
    async signUp(_parent, {fullName, ...rest}) {
        try {
            let [firstName, ...lastName] = fullName.split(" ");
            lastName = lastName.join(" ");

            const user = await User.create({
                firstName,
                lastName,
                ...rest
            });

            await TweetFavorite.create({userId: user._id});

            return {
                token: user.createToken()
            };
        } catch (err) {
            throw err;
        }
    },

    async login(_parent, {username, password}) {
        try {
            const user = await User.findOne({username});

            if (!user) throw new Error("Username doesn't exist!");

            if (!user.authenticateUser(password))
                throw new Error("Password is incorrect");

            return {
                token: user.createToken()
            };
        } catch (err) {
            throw err;
        }
    },

    async me(_parent, _args, {user}) {
        try {
            return await requireAuth(user); // returns instance of type Me
        } catch (err) {
            throw err;
        }
    }
};
