import Tweet from "../../models/Tweet";
import TweetFavorite from "../../models/TweetFavorite";
import {requireAuth} from "../../services/auth";
import {pubSub} from "../../config/pub-sub";

export const TWEET_FAVORITED = "tweetFavorited";

/*
    - fn signature: ( parent, args, ctx )
    - ctx is added when graphqlExpress is called in app.js
*/

export default {
    async getTweet(_parent, {_id}, {user}) {
        try {
            await requireAuth(user);
            return Tweet.findById(_id);
        } catch (err) {
            throw err;
        }
    },

    async getTweets(_parent, _args, {user}) {
        try {
            await requireAuth(user);

            const tweetsQuery = Tweet.find({}).sort({createdAt: -1});
            const favoritesQuery = TweetFavorite.findOne({userId: user._id});

            const [tweets, favorites] = await Promise.all([tweetsQuery, favoritesQuery]);
            console.log(111111, tweets);
            console.log(22222, favorites);

            const tweetsToReturn = tweets.reduce((arr, tweet) => {
                const tweetJson = tweet.toJSON();

                if (favorites.tweets.some(x => x.equals(tweet._id))) {
                    console.log("IS FAVORITED TRUE");
                    arr.push({
                        ...tweetJson,
                        isFavorited: true
                    });
                } else {
                    console.log("IS FAVORITED FALSE");
                    arr.push({
                        ...tweetJson,
                        isFavorited: false
                    });
                }

                return arr;
            }, []);

            return tweetsToReturn;
        } catch (err) {
            throw err;
        }
    },

    async getUserTweets(_parent, _args, {user}) {
        try {
            await requireAuth(user);
            return Tweet.find({user: user._id}).sort({createdAt: -1});
        } catch (err) {
            throw err;
        }
    },

    async createTweet(_parent, args, {user}) {
        try {
            await requireAuth(user);
            return Tweet.create({...args, user: user._id});
        } catch (err) {
            throw err;
        }
    },

    async updateTweet(_parent, {_id, ...rest}, {user}) {
        try {
            // rest is all other args that come in passed along (e.g. modified fields like text)
            await requireAuth(user);

            // get tweet with ctx's user._id and short circuit if not theirs
            const tweet = await Tweet.findOne({_id, user: user._id});

            if (!tweet) {
                throw new Error("Tweet not found");
            }

            // Change each key:value which has been passed in from parms
            // Object.entries fn signature: ( Object : [ [k,v], [k,v], ... ] )
            Object.entries(rest).forEach(([k, v]) => {
                tweet[k] = v;
            });

            return tweet.save();
        } catch (err) {
            throw err;
        }
    },

    async deleteTweet(_parent, {_id}, {user}) {
        try {
            await requireAuth(user);

            // get tweet with ctx's user._id and short circuit if not theirs
            const tweet = await Tweet.findOne({_id, user: user._id});

            if (!tweet) {
                throw new Error("Tweet not found");
            }

            await tweet.remove();

            return {message: "Deleted tweet"}; // In type-defs this is the schema for 'type Status' thats returned for deleteTweet.
        } catch (err) {
            throw err;
        }
    },

    async favoriteTweet(_parent, {_id}, {user}) {
        try {
            await requireAuth(user);
            const favorites = await TweetFavorite.findOne({userId: user._id});
            console.log("FAVORITES", favorites);
            return favorites.userFavoritedTweet(_id);
        } catch (err) {
            throw err;
        }
    },

    tweetFavorited: {
        subscribe: () => pubSub.asyncIterator(TWEET_FAVORITED)
    }
};
