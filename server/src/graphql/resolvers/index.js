/*
    - '../schema' can be thought of signatures
    - Resolver can be thought of the nitty-gritty that actually happens
*/

import TweetResolvers from "./tweet-resolvers";
import UserResolvers from "./user-resolvers";
import GraphQLDate from "graphql-date";
import User from "../../models/User";

export default {
    Date: GraphQLDate,

    Tweet: {
        // the fn gets passed the parent object, i.e. a tweet
        // then destructures the user key, which in Tweet has value: user._id!

        // user: ({user: id}) => User.findById(id)
        user: ({user}) => User.findById(user)
    },

    Query: {
        getTweet: TweetResolvers.getTweet,
        getTweets: TweetResolvers.getTweets,
        getUserTweets: TweetResolvers.getUserTweets,
        me: UserResolvers.me // i.e. GET me
    },

    Mutation: {
        createTweet: TweetResolvers.createTweet,
        updateTweet: TweetResolvers.updateTweet,
        deleteTweet: TweetResolvers.deleteTweet,
        favoriteTweet: TweetResolvers.favoriteTweet,
        signUp: UserResolvers.signUp,
        login: UserResolvers.login
    },

    Subscription: {
        tweetFavorited: TweetResolvers.tweetFavorited
    }
};
