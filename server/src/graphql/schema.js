/*
    Notes:

    ==== Getting started ====
    - Ensure model fields (e.g. _id, text) relate to mongoDB model schema
    - Query is equivalent to GET from REST, its an object which groups multiple GETs
    - schema {} is where you canonically assign your Query-object as the graphQL-query

    ==== Making fields accessible in queries ====
    - Need to specify in the graphQL Tweet schema to make it accessible in a query
    - e.g. text : String
    - e.g. fn( text : String ) : Tweet     <= Tweet is type of returned result

    ==== What about MongoDB? ====
    - Note that there is no interface with MongoDB here, you do that in /resolvers
    - Still need tweet resolvers to actually connect to MongoDB

    ==== ID as a type ====
    - Note ID is a type built into graphQL 
    - It has to be unique

    ==== Required fields ====
    - Exclamation => REQUIRED -- if not present ABORT
    - Update tweet doesn't have ! on text field, as they may want to just update a different field in future

    ==== Query vs Mutation ====
    - Query is just for GET 
    - Mutation is for all other updates (Create, Update, Delete etc)
    - Don't forget to add 'mutation : Mutation' under schema {}

    ==== The need for type Status {} ====
    - For delete, the server returns nothing
    - Therefore its useful to show the status so FE knows what's going on

    ==== Scalars ====
    - Scalars reference the key defined in resolvers
    - These are defined in /graphql/resolvers/index
*/

export default `
    scalar Date 

    type Status {
        message : String!
    }

    type Auth {
        token : String!
    }

    type User {
        _id : ID!
        username : String!
        email : String
        firstName : String
        lastName : String
        avatar : String
        createdAt : Date
        updatedAt : Date
    }

    type Me {
        _id : ID!
        username : String!
        email : String
        firstName : String
        lastName : String
        avatar : String
        createdAt : Date
        updatedAt : Date
    }

    type Tweet {
        _id : ID!
        text : String!
        createdAt : Date!
        updatedAt : Date!
        user : User!
        favoriteCount : Int!
        isFavorited: Boolean
    }


    type Query {
        getTweet( _id : ID! ) : Tweet
        getTweets : [ Tweet ]
        getUserTweets : [ Tweet ]
        me : Me
    }

    type Mutation {
        createTweet( text : String! ) : Tweet
        updateTweet( _id : ID!, text : String ) : Tweet
        deleteTweet( _id : ID! ) : Status
        favoriteTweet( _id : ID!) : Tweet
        signUp( email : String!, fullName : String!, password : String!, username : String!, avatar : String ) : Auth
        login( username : String!, password : String! ) : Auth
    }

    type Subscription {
        tweetFavorited : Tweet
    }

    schema {
        query : Query
        mutation : Mutation
        subscription : Subscription
    }
`;
