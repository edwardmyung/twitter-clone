export default {
    PORT: process.env.PORT || 3000,
    DB_URL:
        "mongodb://main-admin:foobarnavyclone123@ds137435.mlab.com:37435/twitter-clone-development",
    GRAPHQL_PATH: "/graphql",
    JWT_SECRET: "This is a secret"
};
