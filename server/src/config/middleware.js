import bodyParser from "body-parser";
import constant from "./constant";

// graphql deps
import {graphiqlExpress, graphqlExpress} from "apollo-server-express";
import {makeExecutableSchema} from "graphql-tools";
import typeDefs from "../graphql/schema";
import resolvers from "../graphql/resolvers";

import {decodeToken} from "../services/auth";

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

// middleware signature in express-js ( req, res, next )
// If jwt set in header under 'authoirization', decode and set payload (user) onto req
async function auth(req, _res, next) {
    try {
        const token = req.headers.authorization;

        if (token) {
            const user = await decodeToken(token); // throws error if not valid token
            req.user = user;
        } else {
            req.user = null;
        }
        return next();
    } catch (err) {
        throw err;
    }
}

export default app => {
    app.use(bodyParser.json());

    app.use(auth);

    // setup IDE tool for graphQL on localhost:3000/graphiql
    // Note endpointURL is the actual BE url ( i.e. /graphql )
    app.use(
        "/graphiql",
        graphiqlExpress({
            endpointURL: constant.GRAPHQL_PATH
        })
    );

    // setup graphQL server
    app.use(
        constant.GRAPHQL_PATH,
        graphqlExpress(req => ({
            schema,
            context: {
                user: req.user
            }
        }))
    );
};
