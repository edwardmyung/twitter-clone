import mongoose from "mongoose";
import constant from "./constant";

// Prevent deprecation error
mongoose.Promise = global.Promise;

// On insert/create/delete we can see a log in terminal
mongoose.set( "debug", true );

// Connect to DB
try {
    mongoose.connect( constant.DB_URL );
} catch ( err ) {
    mongoose.createConnection( constant.DB_URL );
}

// Listeners for connection
mongoose.connection
    .once( "open", () => console.log( "MongoDB running" ) )
    .on( "error", err => {
        throw err;
    } );