import jwt from "jsonwebtoken";
import User from "../models/User";

import constant from "../config/constant";

export async function requireAuth(user) {
    // If this fails you're definitely un-authed
    if (!user || !user._id) {
        throw new Error("Unauthorized to view this page.");
    }

    // This code handles case where you may have a JWT of a user that no longer exists
    const me = await User.findById(user._id);

    if (!me) throw new Error("Unauthorized");

    return me;
}

export function decodeToken(token) {
    // splits into a keyword, and the token
    const arr = token.split(" ");
    if (arr[0] === "Bearer") {
        // BEARER is prefix to the token, that FE devs will use
        // returns the decoded payload if token is verified
        return jwt.verify(arr[1], constant.JWT_SECRET);
    }

    throw new Error("Token not valid!");
}
