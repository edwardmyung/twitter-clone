import faker from "faker";

import Tweet from "../models/Tweet";
import User from "../models/User";

const TWEETS_COUNT = 10;
const USERS_COUNT = 3;

export default async () => {
    try {
        await Tweet.remove();
        await User.remove();
        
        await Array.from({ length : USERS_COUNT }).forEach( async ( _, ix ) => {
            const user = await User.create({ 
                username : faker.internet.userName(),
                firstName: faker.name.firstName(),
                lastName : faker.name.lastName(),
                email    : faker.internet.email(),
                avatar : `https://randomuser.me/api/portraits/women/${ ix }.jpg`,
                password : "foobar"
            });

            await Array.from({ length : TWEETS_COUNT }).forEach( async () => { 
                await Tweet.create({ 
                    user : user._id,
                    text : faker.lorem.sentence()
                });
            });
        });
    } catch( err ) {
        throw err;
    }
};