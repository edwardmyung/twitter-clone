# Post backend

This README.md is for after Part 5: when the backend is complete

# Part 6: Installing Expo, React-Native

1. npm install exp --global
2. See React-Native install instructions
3. In twitter-clone create a mobile folder with the boiler plate by: git clone https://github.com/EQuimper/twitterclone-mobile-starter.git mobile
4. In /mobile: npm install
5. Terminal 1: exp start
6. Terminal 2: exp ios 
7. Need to update package.json as SDK version is no longer supported ( see: https://docs.expo.io/versions/v28.0.0/workflow/upgrading-expo-sdk-walkthrough ). Ensure package.json and expo's manifest (app.json) match version numbers!

- Expo is like RoR for react-native - e.g. there are libraries to interface with native 
- In app.js:
    - ApolloProvider : Redux provider with Apollo wrapper
    - ThemeProvider : Theme colors defined in src/utils/constant, wrapping app in ThemeProvider means all styled-components get access to those props ${ p => p.thee.WHITE }
- Resolving git sub-module issue. This happens when you clone repo-into-repo and need to do:
    i. git rm cached FOLDER_NAME -f
    ii. git add FOLDER_NAME
    iii. git commit . -m "Folder added"
    iv. git push







# Part 7: Building the Tweet Card

1. Setup: Add /screens folder
2. Setup: Create /screens/HomeScreen.js
3. Create /components/TweetCard.js with:
    - Header
    - Body
    - Footer: uses icons + touchable (see 4. below)
4. yarn add @appandflow/touchable. Instead of passing View to styled(), you can pass imported Touchable, and this changes opacity on pressing! This is important for a really native feel.
5. add a ScrollView into HomeScreen to make the whole view scrollable!


Note: 
- flex-direction on styled-components/native is by default 'column'!
- yarn add @expo/vector-icons to get icons, in FeedCard > Footer (see: expo.github.io/vector-icons)






# Part 8: Navigation - StackNavigator, TabNavigator and DrawerNavigator

*** N.B. Of all sections in the notes, this is the least robust - should still be helpful to get going, but refer to react-navigation docs ***

3 types, and can be nested - REMEMBER a navigator is just a component!

- StackNavigator: When user taps a link, a new screen is put on top of old screens.
- TabNavigator: User navigates to different screens by tapping on tabs along the top or bottom of the screen.
- DrawerNavigator: A slide-out ‘drawer’ contains links to different screens.

P.S. All explained nicely here https://medium.com/async-la/react-navigation-stacks-tabs-and-drawers-oh-my-92edd606e4db

## Creating StackNavigator

1. Add navigation.js to src
2. Use { createStackNavigator } from "react-navigation", note this library moves FAST

## Navigation methods

The navigation prop is passed in to every screen component and can be used as follows:

```
// Navigate to a screen
this.props.navigation.navigate( "Home" );

// force refresh, e.g. if new data coming in
// navigate() does NOT refresh
this.props.navigation.push( "AccountDetails" );

// go back 1 screen in stack
this.props.navigation.goBack();

// go back to first screen in stack
this.props.navigation.popToTop();
```

## Passing params 

```
// On origin-screen, navigate with params
this.props.navigation.navigate('RouteName', { name : "Edward Myung" })

// On destination-screen, getParam
this.props.navigation.getParam( "name", "No name specified" ).
```


### Not using redux

Note: Do NOT store navigation state in redux, as it is almost always unnecessary, and this library will stop supporting it

```
Warning: in the next major version of React Navigation, to be released in Fall 2018, we will no longer provide any information about how to integrate with Redux and it may cease to work. Issues related to Redux that are posted on the React Navigation issue tracker will be immediately closed. Redux integration may continue to work but it will not be tested against or considered when making any design decisions for the library.

Some folks like to have their navigation state stored in the same place as the rest of their application state. Think twice before you consider doing this, there is an incredibly good chance that you do not need to do this!.
```

### Adding navigationOptions to StackNavigator

#### Adding a title 
In the screens folder, for each screen you can add "static navigationOptions = { ... }", where you can pass in 'title' : "Home"

#### Adding a title (but with params)

Or if using params in the title, you need to create a function which returns object as you can't access this.props from static properties!

```
static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam( 'name', 'Home' ), 
    };
  };
}
```

#### Dynamically changing navigationOptions

```
<Button
    title="Update the title"
    onPress={() => this.props.navigation.setParams({otherParam: 'Updated!'})}
/>
```

#### Adding color

There are three key properties to use when customizing the style of your header: headerStyle, headerTintColor, and headerTitleStyle.

1. headerStyle: cssStyle object passed to the wrapper
2. headerTintColor: back button + title
3. headerTitleStyle: cssStyle object passed to the title

```
class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  /* render function, etc */
}
```

#### DRY it out + Simplify

- DRY it out: Instead of putting all color configs in each screen, you can pass it as second object to createStackNavigator() OR createBottomTabNavigator
- Simplify: Still need to add static navigationOptions for titles, which differ across pages... Can add 'navigationOptions' which takes headerTitle (n.b. instead of 'title'!) as well as tabBarIcon









# Part 9 : Connecting front-end to back-end

## Calling graphQl queries from the front-end

1. Create a folder: mobile > graphql/queries. 
2. Add getTweets.js which contains the getTweets query!
3. Import getTweets query to HomeScreen.js, *and* instead of exporting the component, export graphql( GET_TWEETS_QUERY )( HomeScreen ) 
4. Using this graphql function passes the component two keys into a "data" prop:
    i. loading : BOOL
    ii. getTweets : [ Tweets ]  --- key defined by schema
5. Run the back-end, and if needed temporarily comment out requireAuth( user ) in tweet-resolvers > getTweets

## Running the debugger

1. Install react-native-debugger with ( brew update && brew cask install react-native-debugger ) - installs a local application!
2. Add "script" > "debug" in mobile > package.json. Value of "debug" script-key is "open 'rndebugger://set-debugger-loc?host=localhost&port=19001'" --- this is the port you are running expo on.
3. Have app running, and run 'yarn debug' script you just created
4. In expo, press Cmd-D, or Ctrl-Cmd-Z, or open expo menu and click "Debug remote JS"
5. N.b. you can search for the component e.g. "HomeScreen" in the debugger to see the data of that component!

## FlatList vs ScrollView

- Both provided by react-native
- FlatList is more performant
- ScrollView can be useful for animation of small lists
- Now need to pass the data into the TweetCard!







# Part 10 : Sign up on front end

- If you are using Apollo, don't need to use redux for API calls / fetching
- But redux still has its place in managing front-end application state which doesn't relate to API state
- E.g. for state like holding user avatar etc
- It is going to be a very small part of the application
- Can be hard at the start, but once you get it it's a very simple idea

1. Create screens > AuthenticationScreen
2. Create reducers > user.js. Default state includes:
    i. isAuthenticated : BOOL
    ii. info : null OR Object
    iii. token : null or String
3. In Stack Navigator test for user.isAuthenticated and redirect

## Styling components > SignUpForm.js

- iOS Simulator > Hardware > Keyboard - make sure you aren't using hardware as keyboard, otherwise mobile keyboard doesn't come up
- To dismiss keyboard on losing focus, wrap with { TouchableWithoutFeedback, Keyboard } from "react-native", where on press of TouchableWithoutFeedback, you Keyboard.dismiss()
- { Platform } from "react-native" can be tested for "ios" or "android"
- React native input properties:
    - autoCapitalize ("none" or "word")
    - autoCorrect (BOOL)
    - secureText (BOOL) : like type="password"

## Debugging state, and checking state to disable button

- Setup onChangeText fn in SignUpForm.js
- Check SignUpForm.js component on RN Debugger, follow all steps under section "Running the Debugger"!
- Setup checkIfDisabled fn and set disabled prop on button






# Part 11 : Adding SignUp mutation and adding token to async storage

## Calling the mutation and storing result in AsyncStorage

1. Create graphql > mutations > signup.js
2. In components > SignUpForm:
    i. import { graphql } from "react-apollo"
    ii. import SIGNUP_MUTATION from "../graphql/mutations/signup"
    iii. export default ( SIGNUP_MUTATION )( SignUpForm ) 
3. onPress of register, call onSignUpPress
4. In component > SignUpForm : import { AsyncStorage } from "react-native"
5. try/catch with AsyncStorage.setItem( "key-to-represent-app", data.signUp.token )

- N.B. After step (2), when you run React Native Debugger, and open SignUpFrom, you can see that the mutate fn is now a *prop* bound to the component.
- N.B. Don't forget to run the server!

```js
_onSignUpPress = async () => {
    this.setState({ loading : true });
    
    const { fullName, email, password, username } = this.state;
    const avatar = "https://pbs.twimg.com/profile_images/956594738127360000/gzQO5JzI_400x400.jpg";

    const { data } = await this.props.mutate({
        variables : {
            fullName,
            email,
            password,
            username,
            avatar
        }
    });

    try {
        AsyncStorage.setItem( "@twitterclone", data.signUp.token );
        return this.setState({ loading : false });
    } catch ( err ) {
        throw err;
    }
}
```

## Handling loading state

1. Create component > Loading.js
2. In SignUpForm, add 'loading' to state
3. setState({ loading : false }) at start of _signUpPress
4. setState({ loading : true }) at end of _signUpPress
5. Short circuit render if loading is true


## Setting up login reducer, to keep front-end state in sync

1. Create src > actions > user.js, where user.js level files match with reducers
2. export login action-creator fn which returns { type : "LOGIN" }
3. imports in SignUpForm
    i. import { connect } from "react-redux"
    ii. import { login } from "../actions/user";
    iii. import { compose } from "react-apollo" ----- this allows you to compose graphql AND call connect without making a mess at the export - see snippet below
4. In the mapDispatchToProps, pass in { login } where login is action-creator fn
5. In the try {} of onSignUpPress, return this.props.login() so that on completion of signUp, store.js' isAuthenticated is set to true

```js
// using react-apollo compose
import { graphql,, compose } from "react-apollo";
import { connect } from "react-redux";

export default compose(
    graphql( query, queryOptions ),
    graphql( mutation, mutationOptions ),
    connect( mapStateToProps, mapDispatchToProps )
)( Component )
```

## It's hard to see actions in RN Debugger so...

1. yarn add -d redux-logger
2. In store.js: 
    i. import { logger } from "redux-logger"
    ii. add to middlewares array
3. Now in RN Debugger you can see actions in the console!


@25:30


AFTER THIS PART, TIDY CODEBASE AND RE-READ COMMENTS!